# Curso de Prework: Buenas Practicas y Entorno de Desarrollo

## Introducción línea de comandos

### Manejo archivos y directorios

* __ls__ comando que nos permite listar los archivos y directorios de la carpeta donde estemos situados. Se le pueden agregar ciertos parametros  que nos permiten extender la funcioanlidad:
  * __-a__ muestra todos los archivos, hasta los ocultos.
  * __-l__ nos mostrara los permisos, el propietario, la fecha de modificacion, etc.
  * __-t__ ordena por fecha de modificacion.
  > *podemos unir parametros para tener aun mas funcionalidad*
  * __-la__ nos muestra todos los archivos(ocultos tambien) asi como sus permisos, propietario, fecha de modificacion, etc.
  * __-la__ lo mismo que el anterior pero ordenado con fecha de modificacion.
* __clear ó ctrl + l__ limpia pantalla

* __pwd__ nos retorna la ruta absoluta en la que estamos ubicado

> *Ruta absoluta es desde el inicio del sistema de archivo y la ruta relativa es en base a donde estamos ubicados.*

* __mkdir__ crear carpeta
* __history__ guarda el historial de todos los comandos que hemos realizado(!numero)

* __tocuh__ crear archivo(nombre.extension)
* __nano__ abrir archivo

* __mv__ mover un archivo a una carpeta (mv ubicacion_archivo ubicacion_nueva/nombre_archivo)
* __rm__ eliminar archivo 
* __rm -rf nombre_carpeta__ elimina carpeta

### Herramientas basicas (Comandos CAT, MORE, TAIL Y OPEN)

* __CAT__ muestra todo el contenido del archivo en la terminal. Tambien sirve para hacer copia de archivos (cat archivo > nuevo_archivo)
* __MORE__ mismo que CAT pero por partes. Avanzamos linea por linea con INTRO y por pedazos grandes con SPACE, para salir usamos Q.
* __TAIL__ ultimas 10 lineas del archivo. Podemos cambiar el comportamiento de tail agregando un flag con el numero de lineas que queramos ver(-15).
* __OPEN__ sirve para abrir el archivo con el programa que este por defecto para abrirlo(en windoes da problema).

### Crear llaves SSH

__Criptografia__ asimetrica llave publica y llave privada.
__Llaves SSH__ conexion facil y segura a servidores.

__Llave publica(pub)__ la que nos permite navegar segura por internet y conectar a servidores
__Llave privada__ que nos permite desencriptar mensajes

__ssh-keygen -t rsa -b 4098__ crear llaves ssh